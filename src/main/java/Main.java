import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class Main {

    public static void main(String[] args) throws URISyntaxException, NoSuchAlgorithmException, KeyManagementException, IOException, TimeoutException {

        String uri = System.getenv("CLOUDAMQP_URL");
        if(uri == null)
            uri = "amqps://zamiwhek:8kWcKvPObqQyH3cMZPzHcFf1w7ZS6sZl@roedeer.rmq.cloudamqp.com/zamiwhek";

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri);
        factory.setConnectionTimeout(30000);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        boolean durable = false;
        boolean exclusive = false;
        boolean autoDelete = false;

        channel.queueDeclare("monitored_values_queue",durable,exclusive,autoDelete,null);

        List<String> csvValues = ParseCsv.readCsv("sensor.csv");

        for(String i : csvValues){

            if(i!=null){
                ObjectMapper mapper = new ObjectMapper();

                MonitoredValue monitoredValue = new MonitoredValue(
                        Long.parseLong(args[0]),
                        //5L,
                        Instant.now().getEpochSecond(),
                        Double.parseDouble(i)
                );

                String json = mapper.writeValueAsString(monitoredValue);

                channel.basicPublish("", "monitored_values_queue", null, json.getBytes());

                System.out.println(json);

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
