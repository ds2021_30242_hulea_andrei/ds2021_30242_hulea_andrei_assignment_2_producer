import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ParseCsv {

    static List<String> values;

    public static List<String> readCsv(String filename) throws IOException {

        values = new ArrayList<>();

        BufferedReader csvReader = new BufferedReader(new FileReader(filename));

        while(csvReader.readLine() != null)
            values.add(csvReader.readLine());

        return values;
    }

/*    public void readCsv2(String filename) throws IOException {

        BufferedReader csvReader = new BufferedReader(new FileReader(filename));

        while(csvReader.readLine() !=null){
            double value = Double.parseDouble(csvReader.readLine());

            var monitored = new MonitoredValue(Long.getLong("1"), LocalDateTime.now(), value);

        }

    }*/

}
